# React Animenu

React Animenu is a lightweight, customizable, animated navigation menu.

### Installation

```sh
$ npm install react-animenu
```

![](menu-gif.gif)

### Usage

```
import Menu from 'react-animenu';

<Menu
  buttonColorOpen="blue"
  openIcon="X"
>
  <a href="/">Home</a>
  <a href="/about">About</a>
  <a href="/contact">Contact</a>
</Menu>
```

### Props

| Plugin              | Default         | Type                        |
| ------------------- | --------------- | --------------------------- |
| buttonColorOpen     | "yellow"        | string                      |
| buttonColorClosed   | "#006bba"       | string                      |
| iconTextWhenOpen    | "Close"         | string                      |
| iconTextWhenClosed  | "Menu"          | string                      |
| iconTextFont        | "Comic Sans MS" | string                      |
| openIcon            | null            | string, function, component |
| closeIcon           | null            | string, function, component |
| menuBackgroundColor | "red"           | string                      |

### Development

Want to contribute? Please do!

### Todos

- Write Tests
- Add custom animations

## License

MIT
