import React from "react";
import styled from "styled-components";
import posed from "react-pose";
import PropTypes from "prop-types";

export default class Menu extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: false
		};
	}

	handleMenuClick = () => {
		this.setState({
			isOpen: !this.state.isOpen
		});
	};

	renderChildren = () => {
		return React.Children.map(this.props.children, child => {
			return React.cloneElement(child, {
				onClick: this.handleMenuClick
			});
		});
	};

	renderMenuIconText = () => {
		if (!this.props.iconTextWhenOpen || !this.props.iconTextWhenClosed) {
			return;
		}

		return this.state.isOpen
			? this.props.iconTextWhenOpen
			: this.props.iconTextWhenClosed;
	};

	renderMenuOpenIcon = () => {
		if (this.state.isOpen) {
			return typeof this.props.openIcon === "function"
				? this.props.openIcon()
				: this.props.openIcon;
		} else if (!this.state.isOpen) {
			return typeof this.props.closeIcon === "function"
				? this.props.closeIcon()
				: this.props.closeIcon;
		}
	};

	render() {
		return (
			<React.Fragment>
				<StyledMenuAction
					data-testid="actionBtn"
					role="button"
					aria-label={!this.state.isOpen ? "Open menu" : "Close menu"}
					openColor={this.props.buttonColorOpen}
					closedColor={this.props.buttonColorClosed}
					width={this.props.width}
					open={this.state.isOpen}
					onClick={this.handleMenuClick}
					{...this.props}
				>
					{/* Menu icon text */}
					<MenuText font={this.props.font}>
						{this.renderMenuIconText()}
					</MenuText>

					{/* Menu icon */}
					{this.renderMenuOpenIcon()}
				</StyledMenuAction>

				{/* hidden sidebar */}
				<Sidebar
					data-testid="hidden-menu"
					backgroundColor={this.props.menuBackgroundColor}
					// style={{ ...this.props.sideBarStyles }}
					className="sidebar-menu"
					pose={this.state.isOpen ? "open" : "closed"}
					aria-hidden={this.state.isOpen ? "false" : "true"}
				>
					{this.renderChildren()}
				</Sidebar>
			</React.Fragment>
		);
	}
}

// STYLES
const StyledSideBar = styled.ul`
	position: fixed;
	transform: translateX(200%) translateZ(0px);
	width: 100%;
	overflow: hidden;
	background-color: ${props =>
		props.backgroundColor ? props.backgroundColor : "red"};
	background-size: cover;
	background-position: center;
	height: ${props => (props.height ? props.height : "100vh")};
	padding-top: 100px;
	padding-left: 40px;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	list-style: none;
	margin: 0;
	top: 0;
	bottom: 0;
	right: 0;
	z-index: 2;
	box-shadow: 28px 2px 60px 40px black;

	@media (min-width: 768px) {
		width: 40%;
	}
`;

const Sidebar = posed(StyledSideBar)({
	open: {
		x: "0%",
		delayChildren: 100,
		staggerChildren: 80,
		transition: {
			type: "tween",
			duration: 300
		}
	},
	closed: {
		x: "200%",
		delay: 200,
		transition: {
			type: "tween",
			duration: 500
		}
	}
});

const StyledMenuAction = styled.button`
	display: inline-flex;
	position: relative;
	align-items: center;
	justify-content: center;
	background: ${props => (props.open ? props.openColor : props.closedColor)};
	border-radius: 3px;
	border: none;
	padding: 8px;
	width: auto;
	opacity: 1;
	width: ${props => props.width};
	z-index: 999999999;
	cursor: pointer;
	transition: background 0.5s;
`;

const MenuText = styled.span`
	font-family: ${props => props.font};
	text-transform: uppercase;
	z-index: 5;
	transition: color 0.5s;
	color: white;
`;

Menu.propTypes = {
	buttonColorOpen: PropTypes.string,
	buttonColorClosed: PropTypes.string,
	iconTextWhenOpen: PropTypes.string,
	iconTextWhenClosed: PropTypes.string,
	iconTextFont: PropTypes.string,
	openIcon: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.func,
		PropTypes.element
	]),
	closeIcon: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.func,
		PropTypes.element
	]),
	menuBackgroundColor: PropTypes.string
};

Menu.defaultProps = {
	buttonColorOpen: "yellow",
	buttonColorClosed: "#006bba",
	iconTextWhenOpen: "Close",
	iconTextWhenClosed: "Open",
	iconTextFont: "Comic Sans MS",
	openIcon: null,
	closeIcon: null,
	menuBackgroundColor: "red"
};
