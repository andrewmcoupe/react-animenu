import React from "react";
import "@babel/polyfill";

import {
	render,
	fireEvent,
	cleanup,
	waitForElement
} from "react-testing-library";

// add custom jest matchers from jest-dom
import "jest-dom/extend-expect";

import Menu from "../lib/index";

// automatically unmount and cleanup DOM after the test is finished.
afterEach(cleanup);

describe("<Menu />", () => {
	it("should render the hidden menu on click of the action button", async () => {
		// Arrange
		const { getByText, getByTestId, container, asFragment } = render(
			<Menu>
				<p>First menu item</p>
				<p>Second menu item</p>
				<p>Third menu item</p>
			</Menu>
		);

		const actionBtn = getByTestId("actionBtn");

		// Act
		fireEvent.click(actionBtn);

		// Assert
		const hiddenMenu = await waitForElement(() =>
			// getByTestId throws an error if it cannot find an element
			getByTestId("hidden-menu")
		);

		expect(hiddenMenu).toBeInTheDocument();
	});

	it("should render the child elements of the hidden menu on click of the action bar", async () => {
		// Arrange
		const { getByText, getByTestId, container, asFragment } = render(
			<Menu>
				<p data-testid="child1">First menu item</p>
				<p data-testid="child2">Second menu item</p>
				<p data-testid="child3">Third menu item</p>
			</Menu>
		);

		const actionBtn = getByTestId("actionBtn");
		const child1 = getByTestId("child1");
		const child2 = getByTestId("child2");
		const child3 = getByTestId("child3");

		// Act
		fireEvent.click(actionBtn);

		// Assert
		const hiddenMenu = await waitForElement(() =>
			// getByTestId throws an error if it cannot find an element
			getByTestId("hidden-menu")
		);

		expect(child1).toBeInTheDocument();
		expect(child2).toBeInTheDocument();
		expect(child3).toBeInTheDocument();
	});
});
